# Base16 to ST

Converts Base16 themes into Suckless terminal colours

## Usage

`python Base16_to_ST.py <base16_theme.yaml>`

Then replace the section that starts with `static const char *colorname[]` with the output in your `config.def.h`.
