"""
Base16 to ST
Convert Base16 themes to Suckless Terminal themes
By Kat Hamer
"""

import yaml   # YAML parser
import click  # Handle command line arguments

def generate_indexes():
    """Create a generator for base16 formatted hex numbers up to 16"""
    for i in range(16):
        hex_value = hex(i)  # Convert number to hex
        stripped_value = hex(i)[2:]  # Remove leading 0x from hex
        padded_value = stripped_value.zfill(2)  # Pad hex value to make sure it's always 2 digits
        upper_value = padded_value.upper()  # Convert value to uppercase
        yield f"base{upper_value}"  # Yields a value like 'base0E'

def convert_to_st(base16):
    """Convert a Base16 dictionary to ST header code"""
    scheme = base16.get("scheme", "Unknown scheme name")
    author = base16.get("author", "Unknown author")

    base_values = []  # List to store values

    for base in generate_indexes():
        base_value = base16.get(base, "#FFFFFF")
        base_values.append(base_value)

    print(f"/*\nTheme: {scheme}\nAuthor: {author}\n*/\n")  # Print comment for theme details
    print("static const char *colorname[] = {")  # Print start of colour array
    print("    /* 8 normal colors */")
    for base_value in base_values[:8]:  # Generate array for first eight colours
        print(f"    \"#{base_value}\",")
    print("\n    /* 8 bright colors */")
    for base_value in base_values[8:16]:  # Generate array for last eight colours
        print(f"    \"#{base_value}\",")
    print("\n    [255] = 0,\n")
    print("    \"#FFFFFF\",")
    print("    \"#CCCCCC\",")
    print("};")

@click.command()
@click.argument('base16', type=click.File('rb'))
def main(base16):
    """Main function"""
    parsed_file = yaml.safe_load(base16)
    convert_to_st(parsed_file)

if __name__ == "__main__":
    main()  # Run main function
